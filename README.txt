-- SUMMARY --

The Translate nodes module provides an easy way for teams of translators and
supervisors to divide work and utilize Google translate service as a helper.
The UI is helpful for translating as you can see the source field on the left
and destination field on the right, and you can even switch the source language
for some other available translation language.

For a full description of the module, visit the project page:
  http://drupal.org/project/translate_node

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/translate_node


-- FEATURES --

* A UI that makes translating nodes easy
* A workflow that allows easy organizing a team of translators and reviewing
* their work
* Utilizes Google translate per field for assistance
* Excluding unneccecary fields for better focus on the objective (automatic
  exclusion of synchronized fields, and custom list exclusions)
* Internal link URL translations based on the language path aliases
* Translation outdated alert
* Language normalization tool - makes all the source nodes of the same language


-- REQUIREMENTS --

* Content translation (core)


-- RECOMMENDED ADD-ONS --

* CKEditor, makes text, especially with a lot of links, easier to read.
* Path (core), the clean url links will look into the alias table and fetch the
  url alias for the correct language.
* Synchronization (i18n_sync), if used, the synchronized fields will be excluded
  from the translation UI making it easier to focus on what's important.  


-- INSTALLATION --

* Install as usual, see https://www.drupal.org/documentation/install/modules-themes/modules-7
  for further information.


-- CONFIGURATION --

* Assign the 'translate nodes translator' role:

  - Assign the 'translate nodes translator' role to users which will do the
    actual translating, by editing their user accounts in Administration »
    People

* Configure user permissions in Administration » People » Permissions:

  - Translator access

    Users in roles with the "Translator access" permission will be able to
    access the main translation table located at Administration » Regional and
    language » Translate Nodes. Since a supervisor can assign a node only to a
    user with the 'translate nodes translator' role, you need to assign this
    permission to that role, so the translator would be able to access the
    assigned node in the translation interface.

  - Supervisor access

    Users in roles with the "Supervisor access" are able to access the
    translation management options (settings page) in Administration » Regional
    and language » Translate Nodes module settings.

  Note that the menu items displayed in the administration menu depend on the
  actual permissions of the viewing user. For example, the "People" menu item
  is not displayed to a user who is not a member of a role with the "Administer
  users" permission.

* Add previously created nodes eligible for translation:
 
  - This is a one time operation. Do this if you already have content in the
    system. If not, you won't be able to see this option, because you don't need
    it.
    
    Basically, for every already existing node in the source language, that
    doesn't have a translation for some or all of the defined destination
    languages, one will be created. At the same time the main translation table
    will be populated.
    For every new node this will happen automatically.
    
    Located on the settings page: Administration » Regional and language »
    Translate Nodes module settings
  
* Normalize source nodes:

  - If you already have content in the system, you should perform the language
    normalisation. This means all the source nodes (the nodes you wish to
    translate) should be in the same language. You can choose which language
    that would be, or the default site language will be used.
    
    You can use this tool independently whenever you want, to keep things nice
    and neat. Maybe you have content administrators who sometimes create a node
    in one language, translate it to others, and vice versa the next time.
    
    Located on the settings page: Administration » Regional and language »
    Translate Nodes module settings

* Your Google API key

  - For Google translate assist to work you need a Google API key which you can
    get __here__. If you don't have one, the module will still be helpful
    because of the translate interface and organizational workflow.
    
    Located on the settings page: Administration » Regional and language »
    Translate Nodes module settings


-- CUSTOMIZATION --

* The translation interface will show every field that is either textarea or
  textfield except:

  1) if synchronization is enabled, the fields that are synchronized for all
     translations in the Administration » Structure » Content types » {content
     type} - Synchronize translations tab

  2) Fields listed in the Exclude list on the settings page (Administration »
     Regional and language » Translate Nodes module settings)



-- KNOWN ISSUES --

* None, yet.



-- CONTACT --

Current maintainers:
* Vladimir Horvat (fermicoding) - https://drupal.org/user/900934
* Nikola Arezina

This project is sponsored by:
* Fermicoding Internet Engineering
  Specialized in PHP and Drupal development, theming and architecture.
  Visit http://www.fermicoding.com for more information.

* BeRGB
  Design, front end and UI. Visit http://www.bergb.com for more information.
