<?php

/**
 * @file
 * Provide a code for UI pages.
 */

/**
 * Page callback: Displays the nodes and translations status table.
 *
 * @return string
 *   Page markup.
 */
function translate_nodes_nodes_page() {

  global $user;

  $tn_path = drupal_get_path('module', 'translate_nodes');

  drupal_add_css($tn_path . '/css/translate_nodes.admin.css');
  drupal_add_js($tn_path . '/js/translate_nodes.js');

  // Check if current user is a supervisor or not.
  $is_supervisor = $user->uid == 1 || user_access('translate_nodes supervisor access') ? TRUE : FALSE;

  // We want to display the nodes in columns of "languages we want to translate
  // TO".
  $fromlang = _translate_nodes_get_from_lang();
  $langs = array_keys(language_list());
  $langs = array_diff($langs, array($fromlang));

  $header = array(
    array('data' => ' '),
    array('data' => t('Title')),
  );

  foreach ($langs as $lang) {
    $header[] = array('data' => $lang);
  }

  // Get the list of node types that support translation.
  $types = node_type_get_names();
  foreach ($types as $type => $value) {
    if (!translation_supported_type($type)) {
      unset($types[$type]);
    }
  }
  $types = array_keys($types);

  // Get a list of nodes that can be translated.
  $query = db_select('node', 'n')
    ->fields('n', array('nid', 'title', 'tnid'))
    ->condition('n.language', $fromlang)
    ->orderBy('changed', 'DESC')
    ->orderBy('created', 'DESC')
    ->extend('PagerDefault');
  if (!empty($types)) {
    $query->condition('n.type', $types, 'in');
  }
  else {
    $query->condition('n.type', '');
  }

  // If we are not a supervisor (but a translator) extend the query so that we
  // get a list of only the nodes assigned to us.
  if (!$is_supervisor) {

    // Select nids of nodes we are currently translating.
    $subquery2 = db_select('translate_nodes_status', 'tn')
      ->fields('tn', array('nid'))
      ->condition('tn.uid', $user->uid);

    // Get distinct tnids for that list.
    $subquery1 = db_select('node', 'n1')
      ->fields('n1', array('tnid'))
      ->condition('n1.nid', $subquery2, 'in');

    // Revise the original list of nodes properties of selected language to also
    // have these tnids.
    $query = $query->condition('n.nid', $subquery1, 'in');

  }
  $res = $query->execute();

  // Fetch users with the role of 'translate nodes translator'.
  $tuser = db_query("SELECT u.uid, u.name FROM {users} u
                     JOIN {users_roles} ur ON ur.uid = u.uid
                     JOIN {role} r ON r.rid = ur.rid
                     WHERE u.status = 1 AND r.name = 'translate nodes translator'")->fetchAll();

  $rows = array();

  // Table y axis.
  foreach ($res as $key => $source_node) {
    // Select nodes that are direct translation of the $source_node.
    $query = db_select('node', 'n');
    $query->leftJoin('translate_nodes_status', 'ts', 'ts.nid = n.nid');
    $query->fields('n', array('nid', 'title', 'language', 'translate'))
          ->fields('ts', array('status', 'uid'))
          ->condition('n.language', $fromlang, '!=')
          ->condition('n.language', LANGUAGE_NONE, '!=')
          ->condition('n.tnid', $source_node->tnid);
    $res1 = $query->execute();
    $table_data = array();

    // Table x axis.
    while ($node = $res1->fetchAssoc()) {

      // Here we will prepare the value for the drop down items that contain
      // the nid of the node and a uid of the available translator (or -1 for
      // option 'no translator assigned'), so we can parse it later when
      // assigning through ajax.
      $options = array($node['nid'] . '/-1' => t('none'));
      $default_value = $node['nid'] . '/-1';
      foreach ($tuser as $translator) {

        $options[$node['nid'] . '/' . $translator->uid] = $translator->name;
        if ($translator->uid == $node['uid']) {
          $default_value = $node['nid'] . '/' . $translator->uid;
        }

      }

      // Form translators.
      $form = drupal_get_form('translate_nodes_translators_form', $options, $default_value, $node['nid']);
      $prev = drupal_render($form);
      // Form approve.
      $form = drupal_get_form('translate_nodes_approve_form', $node['nid']);
      $push = drupal_render($form);

      // See which translator is assigned for this particular translation node.
      $t_uid = db_query("SELECT uid FROM {translate_nodes_status} WHERE nid = :nid",
                          array(':nid' => $node['nid']))->fetchField();
      if ($t_uid == -1) {
        $t_uid = '';
      }

      // In certain translation node states, we need to block translators from
      // from doing more translation, for example until the supervisor approves
      // the translation.
      $blocked = array(
        'class' => '',
        'title' => '',
      );
      if ($node['uid'] == -1 && !$is_supervisor) {
        $blocked['class'] = ' blocked';
        $blocked['title'] = ' - ' . t('Blocked');
      }
      // Implementing the Content translation modules feature of outdated
      // translations. If someone changes the source node, the others are
      // flagged as outdated. In our translation interface we need to be able
      // to uncheck this flag and say that it has been taken care of.
      $outdated = array(
        'html' => '',
        'title' => '',
      );
      if ($node['translate'] == 1) {
        $outdated['html'] = '<div class="outdated"></div>';
        $outdated['title'] = ' - ' . t('Outdated');
      }

      // If a user is a supervisor or is assigned to this node.
      $can_translate = $is_supervisor || $user->uid == $t_uid;
      $url = 'admin/config/regional/translate-nodes/translate/' . $node['nid'];

      $tmp = '';
      if ($node['status'] == 1) {

        $cl = array('empty', 'nid' . $node['nid'], 'status', $blocked['class']);
        $ti = t('Empty') . $blocked['title'] . $outdated['title'];

        $string1 = l($outdated['html'], $url, array(
          'html' => TRUE,
          'attributes' => array('class' => $cl, 'title' => $ti),
        )) . l(t('Translate'), $url);
        $string2 = '<span class="' . implode(' ', $cl) . '" title="' . $ti . '">' . $outdated['html'] . '</span>';
        $tmp = $can_translate ? $string1 : $string2;
        if ($is_supervisor) {
          $tmp .= $prev;
        }
      }
      elseif ($node['status'] == 2) {

        $cl = array(
          'in-progress',
          'nid' . $node['nid'],
          'status',
          $blocked['class'],
        );
        $ti = t('In progress') . $blocked['title'] . $outdated['title'];

        $string1 = l($outdated['html'], $url, array(
          'html' => TRUE,
          'attributes' => array('class' => $cl, 'title' => $ti),
        )) . l(t('Translate'), $url);
        $string2 = '<span class="' . implode(' ', $cl) . '" title="' . $ti . '">' . $outdated['html'] . '</span>';
        $tmp = $can_translate ? $string1 : $string2;
        if ($is_supervisor) {
          $tmp .= $prev;
        }

      }
      elseif ($node['status'] == 3) {

        $cl = array('done', 'nid' . $node['nid'], 'status');
        $ti = t('Done') . $outdated['title'];

        $string1 = l($outdated['html'], $url, array(
            'html' => TRUE,
            'attributes' => array('class' => $cl, 'title' => $ti),
        )) . l(t('Review'), $url);
        $string2 = '<span class="' . implode(' ', $cl) . '" title="' . $ti . '">' . $outdated['html'] . '</span>';

        $tmp = $can_translate ? $string1 : $string2;
        if ($is_supervisor) {
          $tmp .= $push;
        }

      }
      elseif ($node['status'] == 4) {

        $cl = array('approved', 'nid' . $node['nid'], 'status');
        $ti = t('Approved') . $outdated['title'];

        $string1 = l($outdated['html'], $url, array(
          'html' => TRUE,
          'attributes' => array('class' => $cl, 'title' => $ti),
        )) . l(t('Review'), $url);
        $string2 = '<span class="' . implode(' ', $cl) . '" title="' . $ti . '">' . $outdated['html'] . '</span>';

        $tmp = $is_supervisor ? $string1 : $string2;

      }
      elseif ($node['status'] == 5) {

        $cl = array(
          'need-review',
          'nid' . $node['nid'],
          'status', $blocked['class'],
        );
        $ti = t('Needs review') . $blocked['title'] . $outdated['title'];

        $string1 = l($outdated['html'], $url, array(
          'html' => TRUE,
          'attributes' => array('class' => $cl, 'title' => $ti),
        )) . l(t('Translate'), $url);
        $string2 = '<span class="' . implode(' ', $cl) . '" title="' . $ti . '">' . $outdated['html'] . '</span>';

        $tmp = $can_translate ? $string1 : $string2;
        if ($is_supervisor) {
          $tmp .= $prev;
        }
      }
      $table_data[] = $tmp;

      // Add missing cells.
      $n = $res1->rowCount();
      $m = count($header) - 2;
      for ($i = 0; $i < $m - $n; $i++) {
        $table_data[] = '&nbsp;';
      }
      // End.
    }

    $rows_tmp = array(
      $key + 1,
      l($source_node->title, 'node/' . $source_node->nid),
    );
    $rows_tmp = array_merge($rows_tmp, $table_data);
    $rows[] = array('data' => $rows_tmp);

  }

  $html = '';
  if (variable_get('translate_nodes_base', 3) == 1) {
    $message = t('Before start, you must !add previously created nodes eligible for translation.',
                 array('!add' => l(t('add'), 'admin/config/regional/translate-nodes/settings')));
    drupal_set_message($message, 'warning');
  }
  elseif (variable_get('translate_nodes_base', 3) == 2) {
    $message = t('Language normalization required! Please go to the !settings page and perform language normalization before proceeding.',
                 array('!settings' => l(t('settings'), 'admin/config/regional/translate-nodes/settings')));
    drupal_set_message($message, 'warning');
  }
  $html .= theme('table', array(
    'header' => $header,
    'rows' => $rows,

    // Optional Caption for the table.
    // 'caption' => '',
    // Optional to indicate whether the table headers should be sticky.
    'sticky' => TRUE,

    // Optional empty text for the table if resultset is empty.
    'empty' => t('No nodes for translation...'),
  ));
  $html .= theme('pager', array('tags' => array()));

  return $html;

}

/**
 * Page callback: Displays the translation UI.
 *
 * @param int $nid
 *   Node id of the destination node (eg. the translation node).
 *
 * @return string
 *   Page markup.
 */
function translate_nodes_node_page($nid) {

  $tn_path = drupal_get_path('module', 'translate_nodes');
  drupal_add_css($tn_path . '/css/translate_nodes.admin.css');
  drupal_add_js($tn_path . '/js/translate_nodes.js');
  drupal_add_js(array('translate_nodes' => array('translate_nodes_path' => $tn_path)), 'setting');

  if (function_exists('libraries_get_path') && ($ck_path = libraries_get_path('ckeditor'))) {
    drupal_add_js($ck_path . '/ckeditor.js');
  }

  // Get stuff that we need: language that we are translating from, a
  // translation node that we are going to translate (this may be a new one from
  // the node table or one that is already in the process of translating and in
  // the working table; the source node...
  $fromlang = _translate_nodes_get_from_lang();
  $node = _translate_nodes_get_translation_node($nid);
  $snid = $node->tnid;
  $arg_nid = arg(6);
  if (isset($arg_nid)) {
    $source_node = node_load($arg_nid);
  }
  else {
    $source_node = node_load($snid);
  }

  // And already translated translation nodes that we can use to translate FROM.
  // For example if we have an en node already translated to fr, and are now
  // translating to de, maybe it's easier for the translator to translate from
  // fr instead of en.
  $query = db_select('node', 'n');
  $query->join('translate_nodes_status', 'ts', 'ts.nid = n.nid');
  $query->fields('n', array('nid', 'language'))
        ->condition('n.tnid', $node->tnid)
        ->condition('n.nid', $node->nid, '!=')
        ->condition('ts.status', array(3, 4), 'in');
  $res = $query->execute();

  $title = NULL;
  $type = 'ul';
  $attributes['class'] = array('language-tabs');

  // Make nice tabs containing: the source language...
  $items[] = array('data' => '<span class="active">' . $source_node->language . '</span>');

  // The currently selected from language.
  if ($source_node->language != $fromlang) {
    $items[] = array(
      'data' => l(strtoupper($fromlang),
                  'admin/config/regional/translate-nodes/translate/' . $node->nid . '/' . $node->tnid),
    );
  }

  // And other languages that we can switch to to be the new from language.
  foreach ($res as $tab) {
    if ($source_node->language != $tab->language) {
      $items[] = array(
        'data' => l($tab->language, 'admin/config/regional/translate-nodes/translate/' . $node->nid . '/' . $tab->nid),
      );
    }
  }

  $jez = l(t('Back to table without saving'), 'admin/config/regional/translate-nodes', array('attributes' => array('class' => 'texts-translations'))) .
         '<div class="translations-header">' .
         theme('item_list', array(
           'items' => $items,
           'title' => $title,
           'type' => $type,
           'attributes' => $attributes)) .
         '<span class="language-translating">' . $node->language . '</span></div>';

  $title = $out = '';

  // Exception list - unwanted fields set by the admin on the settings page.
  $tmp = explode(',', str_replace(' ', '', variable_get('translate_nodes_exclude', '')));
  $keys = !$tmp ? '' : array_filter($tmp);

  // If field synchronization is available, add all synchronized fields to the
  // exception list.
  $sync_keys = variable_get('i18n_sync_node_type_' . $node->type, 1);
  if (isset($sync_keys) && is_array($sync_keys)) {
    $keys = array_unique(array_merge($keys, $sync_keys));
  }

  $nodelevi = (array) $source_node;
  $nodedesno = (array) $node;

  // Data-lang attributes are used for JS to attach a google translate function
  // if it is enabled.
  $out .= $jez . '<span class="langs" data-lang="' . $node->language . '" data-lang1="' . $source_node->language . '"></span>';

  $i = 0;
  $node = (array) $node;
  $allowed_tags = '<a><strong><em><span>';

  // Go through the source nodes fields.
  foreach ($nodelevi as $key => $value) {
    $info = field_info_field($key);

    // We really allow for translation only a small set of fields based on these
    // rules.
    if (!in_array($key, $keys) && ($key == 'title' || in_array($info['type'], array(
        'text',
        'text_with_summary',
        'text_long',
    )))) {

      if ($key == 'title') {
        // Dummy array so title key can go through foreach and we don't have to
        // copy the code just for it.
        $value = array(LANGUAGE_NONE => array(0 => array('value' => $value)));
      }
      if (isset($value[LANGUAGE_NONE])) {
        $arr_lnode_lang = LANGUAGE_NONE;
        $arr_dnode_lang = LANGUAGE_NONE;
      }
      elseif (isset($value[$nodelevi['language']])) {
        $arr_lnode_lang = $nodelevi['language'];
        $arr_dnode_lang = $nodedesno['language'];
      }

      $j = 0;
      $textfield = $info['type'] == 'text' || $key == 'title';
      if (isset($value[$arr_lnode_lang])) {
        foreach ($value[$arr_lnode_lang] as $polje) {
          if ($key == 'title') {
            $l_value = strip_tags($polje['value'], $allowed_tags);
            $r_value = strip_tags($nodedesno['title'], $allowed_tags);
            $max_length = 128;
          }
          elseif ($info['type'] == 'text') {
            $l_value = strip_tags($polje['value'], $allowed_tags);
            $r_value = strip_tags($nodedesno[$key][$arr_dnode_lang][$j]['value'], $allowed_tags);
            $max_length = $info['settings']['max_length'];
          }
          else {
            $l_value = filter_xss_admin($polje['value']);
            $r_value = filter_xss_admin($nodedesno[$key][$arr_dnode_lang][$j]['value']);
            $max_length = 0;
          }

          $data[$i] = array(
            'title' => str_replace('_', ' ', check_plain($key)),
            'l_name' => check_plain($key) . $i,
            'r_name' => 'trr/' . $i . '/' . $key . '/' . $arr_dnode_lang . '/' . $j,
            'l_value' => $l_value,
            'r_value' => $r_value,
            'textfield' => $textfield,
            'max_length' => $max_length,
          );

          $j++;
          $i++;
        }
      }
    }
  }

  // Display supervisors set of tools for changing the state of the translation.
  if (user_access('translate_nodes supervisor access')) {
    $res = db_query("SELECT status FROM {translate_nodes_status} WHERE nid = :nid",
                    array(':nid' => $nid))->fetchField();

    $data['status'] = array(
      'options' => array(
        '' => '',
        1 => t('Empty (default)'),
        2 => t('In progress'),
        3 => t('Done'),
        4 => t('Approved'),
        5 => t('Need review'),
      ),
      'default_value' => $res,
    );
  }
  $data['nid'] = $nid;
  $tmp = node_load($nid);
  $data['translation_status']['default_value'] = $tmp->translate;

  $form = drupal_get_form('translate_nodes_translate_ui_form', $data);
  $out .= drupal_render($form);

  return $out;

}

/**
 * Form constructor for the translators form.
 *
 * @param array $options
 *   Items for the select list.
 * @param string $default_value
 *   Default selected nid/uid pair.
 * @param int $nid
 *   A node id.
 *
 * @see translate_nodes_translators_ajax_callback()
 *
 * @ingroup forms
 */
function translate_nodes_translators_form($form, &$form_state, array $options, $default_value, $nid) {

  $form['translators'] = array(
    '#prefix' => '<div id="t' . $nid . '-div">',
    '#suffix' => '</div>',
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => $default_value,
    '#attributes' => array(
      'class' => array('translator', 'nid' . $nid),
    ),
    '#ajax' => array(
      'callback' => 'translate_nodes_translators_ajax_callback',
      'wrapper' => 't' . $nid . '-div',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  return $form;

}

/**
 * Assign a translator to a node.
 *
 * Also send a command that starts a javascript callback. This is a callback for
 * system/ajax initiated from the translators form.
 *
 * @return array
 *   Render array.
 */
function translate_nodes_translators_ajax_callback($form, $form_state) {

  $commands = array();
  if (user_access('translate_nodes supervisor access')) {

    // The value passed by ajax is in format nid/uid. Uid being -1 for no user.
    list($nid, $uid) = explode('/', $form_state['values']['translators']);
    $sup = 0;
    if ($uid != -1) {
      // Check if user has supervisor privilegies.
      if ($uid == 1) {
        $sup = 1;
      }
      else {
        $sup = db_query("SELECT 1 FROM {users_roles} ur
                         JOIN {role} r ON r.rid = ur. rid
                         WHERE ur.uid = :uid AND r.name = :name",
                        array(':uid' => $uid, ':name' => 'translate nodes supervisor'))->fetchField();
      }
      // Check if user is a translator, if role was not removed in the meantime.
      $res = db_query("SELECT 1 FROM {users_roles} ur
                       JOIN {role} r ON r.rid = ur. rid
                       WHERE ur.uid = :uid AND r.name = :name",
                      array(':uid' => $uid, ':name' => 'translate nodes translator'))->fetchField();
    }
    if ((isset($res) && $res == 1) || $uid == -1) {
      // Set the translator for the node language instance.
      $res = db_update('translate_nodes_status')->fields(array(
          'uid' => $uid,
      ))
      ->condition('nid', $nid)
      ->execute();
      if ($res && $res > 0) {
        // Proceed setting up the javascript callback function.
        $commands[] = array(
          'command' => 'afterTranslatorsAjaxCallback',
          'uid' => $uid,
          'nid' => $nid,
          'role' => isset($sup) && $sup == 1 ? 1 : 0,
        );
      }
      else {
        $commands[] = ajax_command_alert(t('Database error: translator not added.'));
      }

    }
    else {
      $commands[] = ajax_command_alert(t('Database error: translator not added.'));
    }

  }

  return array(
    '#type' => 'ajax',
    '#commands' => $commands,
  );

}

/*
 function translate_nodes_translators_form_validate($form, &$form_state) {
}

function translate_nodes_translators_form_submit($form, &$form_state) {
}*/


/**
 * Form constructor for the approve form.
 *
 * @param int $nid
 *   A node id.
 *
 * @see translate_nodes_approve_form_submit()
 *
 * @ingroup forms
 */
function translate_nodes_approve_form($form, &$form_state, $nid) {

  $form = array();
  $form['nidnum'] = array(
    '#type' => 'hidden',
    '#value' => $nid,
  );
  $form['push'] = array(
    '#type' => 'submit',
    '#default_value' => t('push'),
    '#attributes' => array(
      'class' => array('approve'),
    ),
  );

  return $form;

}
/*
 function translate_nodes_approve_form_validate($form, &$form_state) {
}*/

/**
 * Approve the translated node.
 *
 * Approved translated nodes get saved as a normal node and deleted from the
 * working table.
 */
function translate_nodes_approve_form_submit($form, &$form_state) {

  // Shut up pareview.
  $input = 'input';
  // Sometimes these are not equal for some reason.
  $key = $form_state[$input]['nidnum'] != $form_state['values']['nidnum'] ? 'input' : 'values';

  db_update('translate_nodes_status')->fields(array(
  'status' => 4,
  ))
  ->condition('nid', $form_state[$key]['nidnum'])
  ->execute();

  $res = db_query("SELECT value FROM {translate_nodes_node}
                   WHERE nid = :nid",
      array(':nid' => $form_state[$key]['nidnum']))->fetchField();

  $snode = unserialize($res);
  // Set status to published.
  $snode->status = 1;
  node_save($snode);

  db_delete('translate_nodes_node')
  ->condition('nid', $form_state[$key]['nidnum'])
  ->execute();

}

/**
 * Form constructor for the translator ui form.
 *
 * @param array $data
 *   Data needed for form construction.
 *
 * @code
 * $data = array(
 *   // $i is from 0 to how many fields you have
 *   $i => array(
 *     'title'      => 'label'
 *     'l_name'     => 'unique name for the left  field',
 *     'r_name'     => 'unique name for the right field,
 *     'l_value'    => 'left value',
 *     'r_value'    => 'right value',
 *     'textfield'  => FALSE,//is it a textfield or not
 *     'max_length' => 100,
 *   ),
 *   // This should be available if user is supervisor
 *   'status' => array(
 *     'options' => array(
 *         '' => '',
 *         1  => t('Empty (default)'),
 *         2  => t('In progress'),
 *         3  => t('Done'),
 *         4  => t('Approved'),
 *         5  => t('Need review'),
 *     ),
 *     'default_value' => $res_status,
 *   ),
 *   'nid' => $nid;
 *   'translation_status' => array(
 *     // Translate field status
 *     'default_value' => $tmp->translate
 *   )
 * );
 * @endcode *
 *
 * @see translate_nodes_translate_ui_form_submit()
 *
 * @ingroup forms
 */
function translate_nodes_translate_ui_form($form, &$form_state, array $data) {

  $tbl = '<div class="table">';
  foreach ($data as $key => $row) {
    if (!is_numeric($key)) {
      continue;
    }

    $tmp = array();
    $tmp['class'] = array('t-levi');
    $tmp['readonly'] = array('readonly');
    if ($row['textfield']) {
      $tmp['size'] = array('42');
    }
    else {
      $tmp['class'][] = 'ckeditor';
      $tmp['rows'] = array('4');
      $tmp['cols'] = array('40');
    }
    $genabled = variable_get('translate_nodes_apikey', FALSE);
    $gstring1 = t('Google translate');
    $gstring2 = t('Google translate is disabled');
    $form[$row['l_name']] = array(
      '#prefix' => $tbl . '<div class="translate-fields' . ($row['textfield'] ? '' : ' long') . '">',
      '#suffix' => '<div class="translate' . ($genabled ? '' : ' disabled') . '" title="' . ($genabled ? $gstring1 : $gstring2) . '">' . $gstring1 . '</div>',
      '#type' => $row['textfield'] ? 'textfield' : 'textarea',
      '#title' => $row['title'] . ':',
      '#attributes' => $tmp,
      '#default_value' => $row['l_value'],
    );
    // Opening div. Need it just the first time.
    $tbl = '';
    if ($row['textfield']) {
      $form[$row['l_name']]['#maxlength'] = $row['max_length'];
    }

    unset($tmp['readonly']);
    $tmp['class'][0] = 't-desni';
    $form[$row['r_name']] = array(
      '#suffix' => '<div style="clear:both"></div></div>',
      '#type' => $row['textfield'] ? 'textfield' : 'textarea',
      '#attributes' => $tmp,
      '#default_value' => $row['r_value'],
    );
    if ($row['textfield']) {
      $form[$row['r_name']]['#maxlength'] = $row['max_length'];
    }
  }
  $form['nid'] = array(
    '#type' => 'value',
    '#value' => $data['nid'],
  );

  if (user_access('translate_nodes supervisor access')) {
    $form['status'] = array(
      '#prefix' => '</div><div class="control-wrapper"><div class="change-status">',
      '#suffix' => '</div>',
      '#type' => 'select',
      '#options' => $data['status']['options'],
      '#default_value' => $data['status']['default_value'],
    );
  }
  else {
    $form['status'] = array(
      '#prefix' => '</div><div class="control-wrapper"><div class="change-status">',
      '#suffix' => '</div>',
      '#value' => '&nbsp;',
    );
  }
  $form['translation_status'] = array(
    '#prefix' => '<div class="checkbox-outdated">',
    '#suffix' => '</div>',
    '#type' => 'checkbox',
    '#title' => t('This translation needs to be updated'),
    '#default_value' => $data['translation_status']['default_value'],
    '#description' => t('When this option is checked, this translation needs to be updated because the source post has changed. Uncheck when the translation is up to date again.'),
  );
  $form['save'] = array(
    '#prefix' => '<div class="save-text">',
    '#type' => 'submit',
    '#default_value' => t('Save'),
    '#attributes' => array(
      'class' => array('save-translate'),
    ),
  );
  $form['done'] = array(
    '#suffix' => '</div></div>',
    '#type' => 'submit',
    '#default_value' => t('Done'),
    '#attributes' => array(
      'class' => array('done-translate'),
      'title' => array(t('Done')),
    ),
  );

  return $form;

}

/*
 function translate_nodes_translate_ui_form_validate($form, &$form_state) {
}*/

/**
 * Save changes in the translate node, and set the status of the translation.
 */
function translate_nodes_translate_ui_form_submit($form, &$form_state) {
  global $user;

  // Save translation status to a regular node.
  $node = node_load($form_state['values']['nid']);
  if (isset($form_state['values']['translation_status'])) {
    $node->translate = $form_state['values']['translation_status'];
    node_save($node);
  }

  // Set translation status to either a regular node or a translation node.
  $node = _translate_nodes_get_translation_node($form_state['values']['nid']);
  if (isset($form_state['values']['translation_status'])) {
    $node->translate = $form_state['values']['translation_status'];
  }
  $node = (array) $node;

  foreach ($form_state['values'] as $k => $v) {
    /* $params: 0 => 'trr', 1 => $i, 2 => $key, 3 => $lang, 4 => $j */
    $params = explode('/', $k);
    if (count($params) > 1) {
      if ($params[2] == 'title') {
        $node[$params[2]] = $v;
      }
      else {
        $node[$params[2]][$params[3]][$params[4]]['value'] = $v;
        $node[$params[2]][$params[3]][$params[4]]['safe_value'] = $v;
      }
    }
  }
  $node = (object) $node;

  if (isset($form_state['values']['status']) && $form_state['values']['status'] != "") {
    db_update('translate_nodes_status')->fields(array(
    'status' => $form_state['values']['status'],
    ))
    ->condition('nid', $node->nid)
    ->execute();

    if ($form_state['values']['status'] == 4) {
      node_save($node);
      db_delete('translate_nodes_node')
      ->condition('nid', $form_state['values']['nid'])
      ->execute();
    }
  }
  else {
    db_update('translate_nodes_status')->fields(array(
    'status' => $form_state['values']['op'] == 'Save' ? 2 : 3,
    ))
    ->condition('nid', $node->nid)
    ->execute();
  }

  $node->changed = time();
  $node->uid = $user->uid;

  $tnode = serialize($node);
  $date = time();

  $postoji = db_query("SELECT 1 FROM {translate_nodes_node} WHERE nid = :nid",
      array(':nid' => $node->nid))->fetchField();

  if ($postoji == 1) {
    db_update('translate_nodes_node')->fields(array(
    'value' => $tnode,
    'date' => $date,
    ))
    ->condition('nid', $node->nid)
    ->execute();
  }
  else {
    db_insert('translate_nodes_node')->fields(array(
    'nid' => $node->nid,
    'value' => $tnode,
    'date' => $date,
    ))
    ->execute();
  }
  drupal_goto('admin/config/regional/translate-nodes');

}

/**
 * Get a node from the node or the working table.
 *
 * Helper function to get a node based on the nid, from either the node table
 * if translation is complete or from the working table.
 *
 * @param int $nid
 *   Id of the node to get.
 *
 * @return object
 *   Loaded node
 */
function _translate_nodes_get_translation_node($nid) {

  $res = db_query_range("SELECT value FROM {translate_nodes_node} WHERE nid = :nid ORDER BY date DESC", 0, 1,
                        array(':nid' => $nid))->fetchField();

  $snode = "";
  if (!empty($res)) {
    $snode = unserialize($res);
  }

  if (isset($snode) && $snode != '') {
    $node = $snode;
  }
  else {
    $node = node_load($nid);
  }

  return $node;

}
