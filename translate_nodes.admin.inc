<?php

/**
 * @file
 * Provides code for the settings page functionality.
 */

/**
 * Settings page form (admin/config/regional/translate-nodes).
 *
 * @return array
 *   The form structure.
 */
function translate_nodes_settings() {

  // Going to need FAPI ajax functionality.
  drupal_add_library('system', 'drupal.ajax');
  drupal_add_js(drupal_get_path('module', 'translate_nodes') . '/js/translate_nodes.js');

  $languages = language_list();
  foreach ($languages as $k => $lan) {
    $languages[$k] = $lan->name;
  }
  array_unshift($languages, '');

  $form = array();

  if (variable_get('translate_nodes_base', 3) == 1) {

    $form['translate_nodes_import'] = array(
      '#prefix' => '<label for="edit-translate-nodes-import">' . t('Step 1. - Add previously created nodes eligible for translation (this is a one time operation)') . '</label><br />',
      '#suffix' => '<div class="description">' . t('This operation will create related nodes in other languages.') . '</div>',
      '#type' => 'submit',
      '#value' => t('Add'),
      '#submit' => array('translate_nodes_import_submit'),
    );

  }
  else {

    $default_fromlang = variable_get('translate_nodes_fromlang', _translate_nodes_get_from_lang());
    $form['translate_nodes_fromlang'] = array(
      '#type' => 'select',
      '#title' => t('From language - Language normalization'),
      '#options' => $languages,
      '#default_value' => $default_fromlang,
      '#description' => t('Language to translate from. All nodes that you plan to translate should be created in this language. If empty, the site default language will be used.'),
      '#ajax' => array(
        'callback' => 'translate_nodes_autolang_callback',
        'wrapper' => 'autolang-div',
        'method' => 'replace',
        'effect' => 'fade',
      ),
    );

    $form['autolang_container'] = array(
      '#prefix' => '<div id="autolang-div">',
      '#suffix' => '</div>',
    );

    // $form['autolang_container']['autolang_submit'] is added in the form_alter
    // function so no need to repeat ourselves.
  }

  $form['translate_nodes_exclude'] = array(
    '#type' => 'textfield',
    '#title' => t('Exclude list'),
    '#default_value' => variable_get('translate_nodes_exclude', ''),
    '#size' => 40,
    '#description' => t('Write down a comma separated list of textfield or textarea field names you want to exclude from the translation page (ex. title, field_my_custom_field, ...). Synchronised fields are already excluded.'),
  );

  $form['translate_nodes_apikey'] = array(
    '#type' => 'textfield',
    '#title' => t('Your Google API key'),
    '#default_value' => variable_get('translate_nodes_apikey', ''),
    '#size' => 40,
    '#description' => t('Enter your Google API key.'),
  );
  return system_settings_form($form);

}

/**
 * Implements hook_form_alter().
 *
 * Alter the settings form normalization submit button here, since hook_settings
 * has no $form_state var, and ajax needs to rebuild this element based on the
 * input.
 */
function translate_nodes_form_translate_nodes_settings_alter(&$form, &$form_state, $form_id) {

  $default_fromlang = variable_get('translate_nodes_fromlang', _translate_nodes_get_from_lang());
  $languages = language_list();
  foreach ($languages as $k => $lan) {
    $languages[$k] = $lan->name;
  }
  array_unshift($languages, '');

  $step = '';
  if (variable_get('translate_nodes_base', 3) == 2) {
    $step = t('Step 2. -');
  }
  if (variable_get('translate_nodes_base', 3) != 1) {
    $s = isset($form_state['values']['translate_nodes_fromlang']) ? $form_state['values']['translate_nodes_fromlang'] : '';
    $form['autolang_container']['autolang_submit'] = array(
      '#prefix' => '<div id="autolang-div">',
      '#suffix' => '</div>',
      '#type' => 'submit',
      '#value' => t('@step Make source language for all original nodes @language',
        array(
          '@step' => $step,
          '@language' => !empty($s) ? $languages[$s] : $languages[$default_fromlang],
        )),
      '#submit' => array('translate_nodes_autolang_submit'),
    );
  }

}

/**
 * Returns a piece of the form.
 *
 * Callback element needs only select the portion of the form to be updated.
 *
 * Since #ajax['callback'] return can be HTML or a renderable array (or an
 * array of commands), we can just return a piece of the form.
 */
function translate_nodes_autolang_callback($form, $form_state) {

  return $form['autolang_container'];

}

/**
 * A submit handler.
 *
 * A submit handler for the autolang_submit button from the settings form, that
 * initiates a batch process.
 */
function translate_nodes_autolang_submit($form, &$form_state) {

  $fromlang = $form_state['values']['translate_nodes_fromlang'];
  if (!isset($fromlang)) {
    $fromlang = _translate_nodes_get_from_lang();
  }

  $batch = array(
    'title' => t('Normalizing source nodes'),
    'operations' => array(
      array('translate_nodes_normalize', array($fromlang)),
    ),
    'finished' => 'translate_nodes_normalize_finish_callback',
    'file' => drupal_get_path('module', 'translate_nodes') . '/translate_nodes.admin.inc',
  );
  batch_set($batch);
  // Only needed if not inside a form _submit handler.
  // Setting redirect in batch_process.
  // batch_process('node/1');
}

/**
 * Changes the base language.
 *
 * Changes the base language of all the selected source nodes in the system
 * for this batch iteration. This is a Drupal batch pattern function.
 *
 * @param string $lang
 *   Destination language; defaults to English.
 */
function translate_nodes_normalize($lang, &$context) {

  drupal_set_time_limit(0);
  // Limit every request to process maximum of 100 nodes.
  $limit = 100;

  // Batch init. We initialise variables and get the maximum no. of nodes that
  // will need to be processed. This will be run only in the first iteration.
  if (empty($context['sandbox'])) {

    variable_set('translate_nodes_fromlang', $lang);
    translate_nodes_reset_fromlang();
    $context['sandbox']['progress'] = 0;

    $res = db_query("SELECT COUNT(DISTINCT n1.tnid) c
                     FROM {node} n1
                     JOIN {node} n2 ON n1.tnid = n2.tnid
                     WHERE n1.tnid > 0 AND n2.language = :lang AND (n1.tnid != n1.nid OR n1.language != :lang)",
        array(':lang' => $lang))->fetchField();

    $context['sandbox']['max'] = $res;
  }

  try {
    // To change the source language for a node we first need to see which nodes
    // have been translated, and among those which have the source in the wrong
    // language.
    $query = db_select('node', 'n1');
    $query->join('node', 'n2', 'n1.tnid = n2.tnid');
    $query->addField('n1', 'tnid');
    $query->addField('n2', 'nid');
    $query->distinct()
    ->condition('n1.tnid', '0', '>')
    ->condition('n2.language', $lang)
    ->condition(db_or()->condition('n1.tnid', 'n1.nid', '!=')->condition('n1.language', $lang, '!='))
    ->range($context['sandbox']['progress'], $limit);
    $res = $query->execute();
    foreach ($res as $obj) {
      db_update('node')->fields(array(
      'tnid' => $obj->nid,
      ))
      ->condition('tnid', $obj->tnid)
      ->execute();
      $context['sandbox']['progress']++;
      $context['results'][] = $context['sandbox']['progress'] . ' / ' . $context['sandbox']['max'];
    }

  }
  catch (PDOException $e) {
    $context['message'] = $e->getMessage();
    $context['results'][] = $context['sandbox']['progress'] . ' / ' . $context['sandbox']['max'] . ' - ' . $e->getMessage();
  }
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }

}

/**
 * Complete a batch process.
 *
 * Callback for batch_set(). Setts the next phase of the settings wizard.
 */
function translate_nodes_normalize_finish_callback($success, $results, $operations) {

  // The 'success' parameter means no fatal PHP errors were detected. All
  // other error management should be handled using 'results'.
  if ($success) {
    if (variable_get('translate_nodes_base', 3) == 2) {
      variable_del('translate_nodes_base');
    }
    $message = t('Language normalization finished! @res node(s) processed.', array('@res' => count($results)));
    drupal_set_message($message);
  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
        '%error_operation' => $error_operation[0],
        '@arguments' => print_r($error_operation[1], TRUE),
    ));
    drupal_set_message($message, 'error');
  }
  /* Providing data for the redirected page is done through $_SESSION.
   foreach ($results as $result) {
  $items[] = t('Loaded node %title.', array('%title' => $result));
  }
  $_SESSION['my_batch_results'] = $items;*/

}

/**
 * A submit handler for the import_submit button from the settings form.
 *
 * Initiates a batch process.
 */
function translate_nodes_import_submit($form, &$form_state) {

  if (isset($form_state['values']['translate_nodes_fromlang'])) {
    $fromlang = $form_state['values']['translate_nodes_fromlang'];
  }
  else {
    $fromlang = _translate_nodes_get_from_lang();
  }

  $batch = array(
    'title' => t('Creating translations'),
    'operations' => array(
      array('translate_nodes_translateinsert', array('translate', '')),
      array('translate_nodes_translateinsert', array('insert', $fromlang)),
    ),
    'finished' => 'translate_nodes_translateinsert_finish_callback',
    'file' => drupal_get_path('module', 'translate_nodes') . '/translate_nodes.admin.inc',
  );
  batch_set($batch);
  // Only needed if not inside a form _submit handler.
  // Setting redirect in batch_process.
  // batch_process('node/1');
}

/**
 * Prepare data for batch processing.
 *
 * Prepare data for creating related nodes in other languages for existing
 * source nodes that don't have any, or, inserting records in the status table.
 */
function translate_nodes_translateinsert($op, $fromlang, &$context) {

  drupal_set_time_limit(0);

  $limit = 100;
  // Execute only once on the start of the batch task.
  if (empty($context['sandbox'])) {

    if ($op == 'insert') {
      variable_set('translate_nodes_fromlang', $fromlang);
      translate_nodes_reset_fromlang();
    }
    $context['sandbox']['progress'] = 0;

    // Get the list of node types that support translation.
    $types = node_type_get_names();
    foreach ($types as $type => $value) {
      if (!translation_supported_type($type)) {
        unset($types[$type]);
      }
    }
    $context['sandbox']['types'] = array_keys($types);

    // Gel all nodes.
    $query = db_select('node', 'n');
    $query->addField('n', 'nid');

    // That support translation.
    if (!empty($context['sandbox']['types'])) {
      $query->condition('n.type', $context['sandbox']['types'], 'in');
    }
    else {
      // Or none if 0 content types support translation.
      $query->condition('n.type', '');
    }

    $res = $query->execute()->rowCount();
    $context['sandbox']['max'] = $res;

    // We need this custom var so we don't select newly created nodes in the
    // following iterations.
    if ($op == 'translate') {
      $context['sandbox']['begin_time'] = time();
    }
  }

  try {
    $query = db_select('node', 'n');
    $query->addField('n', 'nid');
    $query->range($context['sandbox']['progress'], $limit);

    if ($op == 'translate') {
      $query->condition('n.created', $context['sandbox']['begin_time'], '<');
    }
    if (!empty($context['sandbox']['types'])) {
      $query->condition('n.type', $context['sandbox']['types'], 'in');
    }
    else {
      $query->condition('n.type', '');
    }
    $res = $query->execute();

    foreach ($res as $obj) {
      $node = node_load($obj->nid);
      // For starters just create translate nodes for all other languages.
      // This function will create just the translation nodes that are missing,
      // so if we need a reset, we can just empty the status table, and start
      // over. The second step populates the status table.
      translate_nodes_do_translate_insert($node, $op);
      $context['sandbox']['progress']++;
      $context['results'][] = $context['sandbox']['progress'] . ' / ' . $context['sandbox']['max'];
    }
  }
  catch (PDOException $e) {
    $context['message'] = $e->getMessage();
    $context['results'][] = $context['sandbox']['progress'] . ' / ' . $context['sandbox']['max'] . ' - ' . $e->getMessage();
  }

  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
  else {
    $context['finished'] = 1;
  }

}

/**
 * Complete a batch process.
 *
 * Callback for batch_set().
 */
function translate_nodes_translateinsert_finish_callback($success, $results, $operations) {

  // The 'success' parameter means no fatal PHP errors were detected. All
  // other error management should be handled using 'results'.
  if ($success) {
    if (variable_get('translate_nodes_base', 3) == 1) {
      variable_set('translate_nodes_base', 2);
    }
    $message = t('Translate nodes created. Import in the status table finished. @res node(s) processed.', array('@res' => count($results)));
    drupal_set_message($message);
  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments. Please try again.', array(
        '%error_operation' => $error_operation[0],
        '@arguments' => print_r($error_operation[1], TRUE),
    ));
    drupal_set_message($message, 'error');
    db_delete('translate_nodes_status');
  }
  /* Providing data for the redirected page is done through $_SESSION.
   foreach ($results as $result) {
  $items[] = t('Loaded node %title.', array('%title' => $result));
  }
  $_SESSION['my_batch_results'] = $items;*/

}


/* if fromlang is implemented as a dropdown list validation is not needed
 function translate_nodes_settings_validate($form, &$form_state) {
$maxdisp = $form_state['values']['translate_nodes_fromlang'];
if (!is_numeric($maxdisp))
  form_set_error('translate_nodes_fromlang', t('You must enter an integer for
      the maximum number of links.'));
elseif ($maxdisp <= 0)
form_set_error('translate_nodes_fromlang', t('Maximum number of links must be
    positive.'));
}*/

/*/////////// SETTINGS end //////////*/

/**
 * Prepare for  fromlang cache reset.
 *
 * The next time _translate_nodes_get_from_lang runs it will not take the value
 * from the cache.
 *
 * @return string
 *   This is here just to shut up pareview.
 */
function translate_nodes_reset_fromlang() {
  static $fromlang;

  $fromlang = 'reset';

  return $fromlang;
}
