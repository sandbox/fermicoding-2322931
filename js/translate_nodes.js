/**
 * @file
 * Core behaviour for the Translate Nodes.
 *
 * Callbacks for the ajax functions.
 */

(function ($) {
  "use strict";

Drupal.behaviors.translateNodes = {
  attach: function (context, settings) {
    $('body').once('translateNodes', function() {

      /**
       * Returns the keys of an input object.
       *
       * @param input
       *   An input object.
       * @returns output
       *   An array of keys.
       */
      function arrayKeys(input) {
        var output = new Array();
        var counter = 0;
        var i;
        for (i in input) {
          output[counter++] = i;
        }
        return output;
      }

      // Initialize CKEditor for all textareas, if available.
      if (typeof CKEDITOR != 'undefined') {
        CKEDITOR.config.toolbar = [['Link','Unlink','Format'],];
        CKEDITOR.config.contentsCss = Drupal.settings.pathPrefix + Drupal.settings.translate_nodes_path + '/css/translate_nodes.ckeditor.css';
        CKEDITOR.config.allowedContent = true;
        // CKEDITOR.instances['edit-body2'].setReadOnly();
        CKEDITOR.on('instanceReady', function() {
          var inst = arrayKeys(CKEDITOR.instances);
          for (var i = 0; i < inst.length; i = i + 2) {
            CKEDITOR.instances[inst[i]].setReadOnly();
          }
          $('.cke_contents').css('height','250px');
          $('.grippie').hide();
        });
      }

      if (typeof Drupal.ajax != 'undefined') {
        Drupal.ajax.prototype.commands.afterTranslatorsAjaxCallback = function(ajax, response, status) {
          /* no porpose for now */
        };
      }

      // Call the translate function and set the resulting text in the right
      // field (and CKEDITOR field if enabled).
      $('.translate', context).click(function() {
        if ($(this).hasClass('disabled')) {
          return;
        }
        var from  = $('.langs').attr("data-lang1");
        var to    = $('.langs').attr("data-lang");
        var text  = $('.t-levi', $(this).parent()).val();
        var $desno = $(this);

        $.post(Drupal.settings.basePath + Drupal.settings.pathPrefix + "translate-nodes/translate-field", { from: from, to: to, text: text }).done(function(data) {
          var $desni = $('.t-desni', $desno.parent());
          $desni.val(data);
          if (typeof CKEDITOR != 'undefined' && $('textarea', $desno.next()).length > 0) {
            CKEDITOR.instances[$desni.attr('id')].setData(data);
          }
        });
      });

    });

    // Prevent blocked links from doing stuff.
    var $blocked = $('.page-admin-config-regional-translate-nodes .blocked', context);
    if ($blocked.length > 0) {
      $blocked.css('cursor', 'default').click(function(e) {
        if ($(this).hasClass('blocked')) {
          e.preventDefault();
        }
      });
    }

  }
};

})(jQuery);
